<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $fillable =[
        'user_id','post_id','votes','views'
    ];


    public function post(){
        return $this->belongsTo('App\Post');
    }
}
