<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
use App\Stat;

class Post extends Model
{
    protected $fillable =[

        'category_id','slug','body','title','user_id'

    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
     
}
