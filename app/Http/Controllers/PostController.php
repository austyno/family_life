<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Stat;
use Auth;
use Session;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::orderBy('created_at','desc')->paginate(10);
        //$stat = Stat::all();

        //dd($stat);

        return view('forum.home')->with('post',$post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
       
         $post = Post::where('slug',$slug)->first();
         
        $viewed = Session::get('viewed_post',[]);

            if(!in_array($post->id,$viewed)){

                $post->increment('views');
                Session::push('viewed_post',$post->id);
            }

        return view('forum.single')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function vote($post,$flag)
    {
        
        //dd($post, $flag);
        //check if user has voted for this post b4       
        if(Stat::where('post_id',$post)->where('user_id',Auth::user()->id)->exists()){
           
            return redirect()->back();

        }else{
           switch($flag){
               case 'up':
                Stat::create([
                    'user_id' => Auth::user()->id,
                    'post_id' => $post,
                    'votes' => 1
                ]);
                return redirect()->back();

                    break;

                case 'down':
                Stat::create([
                    'user_id' => Auth::user()->id,
                    'post_id' => $post,
                    'votes' => 0,
                ]);
                return redirect()->back();
                    break;
           }


        }
    }
}
