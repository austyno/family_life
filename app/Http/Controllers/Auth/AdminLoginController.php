<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

   public function __construct(){
        $this->middleware('guest:admin');

    }

    public function showLoginForm()
    {
        return view('auth.admin-login');
    }

    public function Login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $credentials =[
            'email' => $request->email,
            'password' => $request->password
        ];
        
        if(Auth::guard('admin')->attempt($credentials,$request->remember))
        {
            return redirect()->intended(route('admindashboard'));
        }

        return redirect()->back()->withInput($request->all());

    }
}
