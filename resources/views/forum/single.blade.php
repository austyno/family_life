@extends('../layouts.main')
<?php use App\Stat; ?>
@section('content')

<div class="container">
        <div class="row">
            <div class="col-lg-8 breadcrumbf">
                <a href="#">Home</a> <span class="diviver">&gt;</span> <a href="#">General Discussion</a> <span class="diviver">&gt;</span> <a href="#">Topic Details</a>
            </div>
        </div>
    </div>

<div class="post beforepagination">
        <div class="topwrap">
            <div class="userinfo pull-left">
                <div class="avatar">
                        <img src="{{asset($post->user->avatar)}}" alt="" height="40px" />
                         
                        @if(Auth::guard('web')->check())
                            <div class="status green">&nbsp;</div>
                        @else
                            <div class="status red">&nbsp;</div>
                        @endif
                        <div>
                            <p> {{$post->user->username}}
                            <p><strong>Posts</strong> {{ $post->user->posts->count() }}</p>
                        </div>
                </div>

                <div class="icons">
                    <img src="images/icon1.jpg" alt="" /><img src="images/icon4.jpg" alt="" /><img src="images/icon5.jpg" alt="" /><img src="images/icon6.jpg" alt="" />
                </div>
            </div>
            <div class="posttext pull-left">
            <h2>{{$post->title}}</h2>
            <p style="line-height:30px;font-size:18px">{{$post->body}}</p>
            </div>
            <div class="clearfix"></div>
        </div>                              
        <div class="postinfobot">

            <div class="likeblock pull-left">
            <div class="up"><i class="fa fa-eye"></i> &nbsp;{{ $post->views }}</div>
            <a href="{{ route('vote',['post'=>$post->id,'flag'=>'up']) }}" class="up"><i class="fa fa-thumbs-o-up">&nbsp; {{ Stat::where('post_id',$post->id)->where('votes',1)->get()->count() }}</i></a>
            <a href="{{ route('vote',['post'=>$post->id,'flag'=>'down']) }}" class="down"><i class="fa fa-thumbs-o-down"></i>&nbsp;{{ Stat::where('post_id',$post->id)->where('votes',0)->get()->count() }}</a>
            
            </div>

            <div class="prev pull-left">                                        
                <a href="#"><i class="fa fa-reply"></i></a>
            </div>

        <div class="posted pull-left"><i class="fa fa-clock-o"></i> Posted on : {{$post->created_at}}</div>

            <div class="next pull-right">                                        
                <a href="#"><i class="fa fa-share"></i></a>

                <a href="#"><i class="fa fa-flag"></i></a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div><!-- POST -->
@endsection