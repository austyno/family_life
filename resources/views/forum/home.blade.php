@extends('../layouts.main')
<?php use App\Stat; ?>
@section('content')


                <!-- POST -->
                @foreach($post as $p)
                    <div class="post">
                        <div class="wrap-ut pull-left">
                            <div class="userinfo pull-left">
                                <div class="avatar">
                                    <img src="{{asset($p->user->avatar)}}" alt="" height="40px" />
                                        @if(Auth::guard('web')->check())
                                            <div class="status green">&nbsp;</div>
                                        @else
                                            <div class="status red">&nbsp;</div>
                                        @endif
                                </div>

                                <div class="icons">
                                    <img src="{{asset('assets/images/icon1.jpg')}}" alt="" /><img src="images/icon4.jpg" alt="" />
                                </div>
                            </div>
                            <div class="posttext pull-left">
                            <h2><a href="{{route('single',['slug'=>$p->slug])}}">{{$p->title}}</a></h2>
                            <p>{{substr($p->body,0,200)}}..</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="postinfo pull-left">
                            <div class="comments">
                                <div class="commentbg">
                                        {{Stat::where('post_id', $p->id)->get()->count('votes')}}
                                    <br>
                                    <div class="mark"></div>
                                    Votes
                                </div>
                                
                            </div>
                        <div class="views"><i class="fa fa-eye"></i> &nbsp;{{$p->views}}</div>
                        <div class="time"><i class="fa fa-clock-o"></i> {{$p->created_at->diffForHumans()}}</div>                                    
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- POST -->
                @endforeach
@endsection