<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->

<!------ Include the above in your HEAD tag ---------->
<style>
.login-block {
background: #118ec6; 
background: -webkit-linear-gradient(to bottom, #0b85ba, #118ec6);  
background: linear-gradient(to bottom, #8dd2ef, #118ec6); 
float:left;
width:100%;
padding : 50px 0;
}
.container{
    background:#fff; 
    border-radius: 10px; 
    box-shadow:15px 20px 0px rgba(0,0,0,0.1);
}
.login-sec{
    padding: 50px 30px; 
    position:relative;
}
.login-sec .copy-text{
    position:absolute;
    width:80%; 
    bottom:20px; 
    font-size:13px; 
    text-align:center;
}
.login-sec .copy-text i{
    color:#118ec6;
}
.login-sec .copy-text a{
    color:#118ec6;
}
.login-sec h2{
    margin-bottom:30px; 
    font-weight:800; 
    font-size:30px; 
    color: #118ec6;
}
.login-sec h2:after{
    content:" "; 
    width:100px; 
    height:5px; 
    background:#118ec6; 
    display:block; 
    margin-top:20px; 
    border-radius:3px; 
    margin-left:auto;
    margin-right:auto
}
.btn-login{
    background: #118ec6; 
    color:#fff; 
    font-weight:600;
}
.banner-text{
    width:70%; 
    position:absolute; 
    bottom:40px; 
    padding-left:20px;
}
.banner-text h2{
    color:#fff; 
    font-weight:600;
}
.banner-text h2:after{
    content:" "; 
    width:100px; 
    height:5px; 
    background:#118ec6; 
    display:block; 
    margin-top:20px; 
    border-radius:3px;
}
.banner-text p{
    color:#fff;
    font-weight:200%;
    font-size:30px;
}

@media only screen and (max-width: 700px){
   .banner-text{
       position: absolute;    
   }
   .banner-text p{
      height: auto;
      font-size:15px;
      position: absolute;
      
     
   }
   .banner-text h2{
       font-size: 15px;
       
   }
}
@media only screen and (max-width:980px){
    .banner-text{
       position: absolute;
       margin-bottom: 55%; 
   }
   .banner-text p{
      height: auto;
      font-size:18px;
   }
   .banner-text h2{
       font-size: 18px;
   }
   .banner-sec{
       
   }
   .banner-sec img{
       height: 60%;
   }
}


/*.banner-sec{
    background : url( '{{ asset('assets/images/family2.jpg')}}') no-repeat;
    background-size: cover;
}*/
</style>
</head>
<section class="login-block">
        <div class="container">
    <div class="row">
            <div class="col-md-4 login-sec">
                    <div class="card">
                        <div class="card-header">{{ __('Register') }}</div>
        
                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
        
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
        
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
        
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
        
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
        
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            <div class="col-md-8 banner-sec">
                <img src="{{ asset('assets/images/family2.jpg')}}" class="img-responsive" style="width:100%">
                <div class="banner-text">
                    <h2>Prov 11:14</h2>
                        <p> <i> "Where no counsel is, the people fall: but in the multitude of counsellors there is safety."</i></p>
                </div>
            </div>
    </div>
</section>
</html>
