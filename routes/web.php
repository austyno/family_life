<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('forum.index');
// });

Auth::routes();

//   Route::get('/', 'HomeController@index')->name('home');

// Route::get('/', [
//     'uses' => 'PostController@create',
//     'as' => 'userlogin'
// ]);

Route::get('/',[
    'uses' => 'PostController@index',
    'as' => 'home'
]);
Route::get('single/{slug}',[
    'uses' => 'PostController@show',
    'as' => 'single'
]);
Route::get('vote/{id}/{flag}',[
    'uses' => 'PostController@vote',
    'as' => 'vote'
    
]);

Route::prefix('admin')->group(function(){

    Route::get('/login',[
        'uses' => 'Auth\AdminLoginController@showLoginForm',
        'as' => 'adminlogin'
    ]);
    Route::post('/login', [
        'uses' => 'Auth\AdminLoginController@login',
        'as' => 'adminloginsubmit'
    ]);
    Route::get('/',[
        'uses' => 'AdminController@index',
        'as' => 'admindashboard'
    ]);
});
